import React, {Component} from 'react';
import fetchJsonp from 'fetch-jsonp';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      users: {}
    };
  }

  getUsers() {
    fetchJsonp('http://api.gymfire.com/v01/testApi.ashx')
    .then((response) => {
      return response.json()
    })
    .then((result) =>{
      this.setState({ users: result})
    })
    .catch(err => console.error(err))
  }
  
    componentDidMount() {
     this.getUsers();
  }

  render() {
    return (
      <div>
        <div className="col height-50 align-items">
          <div className="text-center ">
            <img
              src="https://randomuser.me/api/portraits/women/82.jpg"
              className="rounded-circle"
              alt=""
            />
          </div>
          <h1 className="text-center">
            {this.state.users.firstname} {this.state.users.lastname}
          </h1>
        </div>

        <div className="w-100 border-top"/>
        <div className="col padding_bottom_part height-50">
          <div className="bottom_div">
            <div className="row align-items-start">
              <div className="col font">
                <b>Sex:</b>
              </div>
              <div className="col">{this.state.users.sex}</div>
              <div className="w-100" />
              <div className="col font">
                <b>Email:</b>
              </div>
              <div className="col">{this.state.users.email}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
